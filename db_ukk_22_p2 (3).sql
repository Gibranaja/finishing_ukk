-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Mar 2023 pada 10.52
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ukk_22_p2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_fasilitas`
--

CREATE TABLE `tb_fasilitas` (
  `id_fakam` int(11) NOT NULL,
  `tipe_kamar` varchar(50) NOT NULL,
  `fasilitas_kamar` varchar(150) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_fasilitas`
--

INSERT INTO `tb_fasilitas` (`id_fakam`, `tipe_kamar`, `fasilitas_kamar`, `gambar`) VALUES
(6, '2', 'jkhjh', ''),
(7, '2', 'b', ''),
(8, '2', 'qwe', ''),
(10, '2', 'q', ''),
(11, '2', 'aq', ''),
(13, '2', 'test', ''),
(14, '2', 'test', ''),
(15, '2', 'qwerty', ''),
(16, '1', 'test', ''),
(17, '1', 'qwerty', ''),
(18, '2', 'test', ''),
(19, '2', 'test', ''),
(20, '2', 'test', ''),
(21, '2', 'test', ''),
(22, '2', 's', ''),
(23, '2', 's', ''),
(24, '1', 'd', ''),
(25, '1', 'f', ''),
(26, '1', 'g', ''),
(27, '1', 'q', ''),
(28, '1', 'w', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_fasilitas_umum`
--

CREATE TABLE `tb_fasilitas_umum` (
  `id_fasilitas_umum` int(11) NOT NULL,
  `nama_fasilitas` varchar(150) NOT NULL,
  `keterangan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_fasilitas_umum`
--

INSERT INTO `tb_fasilitas_umum` (`id_fasilitas_umum`, `nama_fasilitas`, `keterangan`) VALUES
(1, 'Lobby1', 'op');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kamar`
--

CREATE TABLE `tb_kamar` (
  `id_kamar` int(11) NOT NULL,
  `tipe_kamar` varchar(50) NOT NULL,
  `jml` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_kamar`
--

INSERT INTO `tb_kamar` (`id_kamar`, `tipe_kamar`, `jml`) VALUES
(1, 'Deluxe', 36),
(2, 'Superior', 38);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pemesanan`
--

CREATE TABLE `tb_pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `nm_pemesanan` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  `nm_tamu` varchar(150) NOT NULL,
  `id_kamar` int(11) NOT NULL,
  `cek_in` date NOT NULL,
  `cek_out` date NOT NULL,
  `jml` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_pemesanan`
--

INSERT INTO `tb_pemesanan` (`id_pemesanan`, `nm_pemesanan`, `email`, `no_hp`, `nm_tamu`, `id_kamar`, `cek_in`, `cek_out`, `jml`) VALUES
(17, 'd', 'd', '2323', 'd', 2, '2022-05-10', '2022-05-11', 1),
(18, 'fdfdd', 'fj@gmail.com', '86866', 'fd', 2, '2022-05-10', '2022-05-13', 1),
(19, 're', 're', '3434', 're', 1, '2022-05-10', '2022-05-13', 1),
(20, 'qwerty', 'fj@gmail.com', '1323', 're', 1, '2022-05-11', '2022-05-11', 1),
(21, 'bayu', 'bayu@gmail.com', '089898', 'bayu', 1, '2022-05-10', '2022-05-27', 12),
(22, '', '', '', '', 1, '0000-00-00', '0000-00-00', 0),
(23, 'gibran', 'as@gmail.com', '08917219', 'gibran', 2, '2022-05-10', '2022-05-11', 13),
(24, 'test', 'testaja@gmail.com', '082322', 'rew', 2, '2022-05-12', '2022-05-14', 1),
(27, '', '', '', '', 1, '0000-00-00', '0000-00-00', 0),
(37, 'asdasdasd', 'asdas@gmail.com', '09876768768', 'bhbjbk', 2, '2022-05-19', '2022-05-20', 12),
(38, 'asdasdasd', 'asdas@gmail.com', '09876768768', 'bhbjbk', 2, '2022-05-19', '2022-05-20', 12),
(39, 'dsf', 'dssds', '099887', 'qw', 1, '2022-05-26', '2022-05-12', 1),
(41, ' yt', 'yt@gmail.com', '0887877', 'yt', 2, '2022-05-21', '2022-05-28', 9),
(42, ' yt', 'yt@gmail.com', '0887877', 'yt', 1, '2022-05-21', '2022-05-28', 9),
(43, 'nami', 'nami@gmail.com', '089788', 'nami', 2, '2022-05-12', '2022-05-14', 70),
(44, 'bata', 'bata@gmail.com', '080878', 'bata', 2, '2022-05-13', '2022-05-20', 15),
(45, 'poco', 'poco@gmail.com', '0988767', 'poco', 1, '2022-05-12', '2022-05-21', 16),
(46, 'malika', 'malika@gmail.com', '0283283', 'malika', 2, '2022-05-12', '2022-05-28', 18),
(47, 'dasa', 'dasa', '0899', 'qwerty', 2, '2022-05-12', '2022-05-28', 19),
(48, 'daxa', 'daxa', '453432', 'daxa', 2, '2022-05-12', '2022-05-20', 1),
(49, 'Test', 'test@gmail.com', '085346713897', 'Test', 2, '2022-02-10', '2022-02-10', 12),
(50, 'askdja', 'lkasjdlkj@gmail.com', 'alksjd', 'kjdkals', 2, '2022-12-12', '2022-12-10', 12),
(51, 'Test', 'test@gmail.com', '085346713897', 'Testing', 2, '2022-05-24', '2022-05-20', 1),
(52, '', '', '', '', 1, '0000-00-00', '0000-00-00', 0),
(53, 'aslkdj', 'salkdj@gmail.com', '085346713897', 'Aji', 2, '2022-02-12', '2022-02-12', 12),
(54, 'fajta', 'fajta', '13232423', 'fajta', 2, '2022-05-21', '2022-05-26', 1),
(55, 'desk', 'desk@gmail.com', '323222', 'as', 2, '2022-05-18', '2022-05-25', 12),
(56, 'iou', 'iou@gmail.com', '232343', 'iou', 1, '2022-05-26', '2022-05-27', 14),
(57, 'gandy', 'gandy@gmail.com', '018219872', 'gandy', 2, '2022-05-12', '2022-05-19', 15),
(58, 'pou', 'pou@gmail.com', '544343pou', 'pou', 2, '2022-05-05', '2022-05-13', 1),
(59, 'tayo', 'tayo@gmail.com', '988877868', 'tayo', 2, '2022-05-12', '2022-05-21', 1),
(60, 'joe', 'joe@gmail.com', '1212112', 'qwery', 2, '2022-05-13', '2022-05-27', 1),
(61, 'cancel', 'cancel@gmail.com', '433533', 'qwerty', 1, '2022-05-12', '2022-05-20', 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `role` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `pass`, `role`) VALUES
(1, 'jungle', 'gibrandum69', 1),
(2, 'vincent', 'gibrandum69', 2);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_fasilitas`
--
ALTER TABLE `tb_fasilitas`
  ADD PRIMARY KEY (`id_fakam`);

--
-- Indeks untuk tabel `tb_fasilitas_umum`
--
ALTER TABLE `tb_fasilitas_umum`
  ADD PRIMARY KEY (`id_fasilitas_umum`);

--
-- Indeks untuk tabel `tb_kamar`
--
ALTER TABLE `tb_kamar`
  ADD PRIMARY KEY (`id_kamar`);

--
-- Indeks untuk tabel `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_fasilitas`
--
ALTER TABLE `tb_fasilitas`
  MODIFY `id_fakam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `tb_fasilitas_umum`
--
ALTER TABLE `tb_fasilitas_umum`
  MODIFY `id_fasilitas_umum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tb_kamar`
--
ALTER TABLE `tb_kamar`
  MODIFY `id_kamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
