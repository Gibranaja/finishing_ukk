<?php
    require_once("config/connection.php");
    session_start();
    if(isset($_SESSION["nm_pemesanan"])){

  
?>
<!DOCTYPE html>
<html>

<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Hotel Hebat Application|Pemesanan</title>

    <link href="public/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="public/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="public/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="public/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="public/img/favicon.png" rel="icon" type="image/png">
	<link href="public/img/favicon.ico" rel="shortcut icon">

	
    <link rel="stylesheet" href="public/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="public/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="public/css/main.css">


	<!-- sweetalert -->
	<link rel="stylesheet" href="public/css/lib/bootstrap-sweetalert/sweetalert.css">
	<link rel="stylesheet" href="public/css/separate/vendor/sweet-alert-animations.min.css">

    
    
</head>
<body class="with-center-menu">

	<div class="page-content">
		<div class="container-fluid">
        <table id="table-sm" class="table table-bordered table-hover table-sm">
				
				<tr>
					<th>Nama Pemesanan</th>
					<th>Email</th>
					<th>No Hp</th>
					<th>Nama Tamu</th>
					<th>Cek In</th>
					<th>Cek Out</th>
					<th>Jumlah Kamar</th>
				</tr>
				<tr>
					<td><?php echo $_SESSION["nm_pemesanan"] ?></td>
					<td><?php echo $_SESSION["nm_pemesanan"]; ?></td>
					<td><?php echo $_SESSION["email"]; ?></td>
					<td><?php echo $_SESSION["no_hp"]; ?></td>
					<td><?php echo $_SESSION["nm_tamu"]; ?></td>
					<td><?php echo $_SESSION["cek_in"]; ?></td>
					<td><?php echo $_SESSION["cek_out"]; ?></td>
					<td><?php echo $_SESSION["jml"]; ?></td>
				</tr>
			</table>
		
		</div><!--.container-fluid-->  
	</div><!--.page-content-->

    <script src="public/js/lib/jquery/jquery.min.js"></script>
	<script src="public/js/lib/tether/tether.min.js"></script>
	<script src="public/js/lib/bootstrap/bootstrap.min.js"></script>
	<script src="public/js/plugins.js"></script>
    <script src="public/js/app.js"></script>
    <script>
        window.print();
    </script>
</body>
</html>
<?php
	  session_abort();
    }else{
        header("Location:".BASE_URL);
    }
?>