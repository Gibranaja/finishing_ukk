<?php
    require_once '../../config/connection.php';
    session_start();
    if(isset($_SESSION["id_user"]) && ($_SESSION["role"] !='2')){ 
        require_once '../../modals/TipeKamar.php';
        $tipekamar = new TipeKamar();
        $result = $tipekamar->get_data();

?>
<?php
    require_once("../layoutPartial/head_template.php");
?>
<body class="with-side-menu">

<?php
    require_once("../layoutPartial/header.php");
?>
	

	<div class="mobile-menu-left-overlay"></div>
<?php
    require_once("../layoutPartial/nav.php");
?>
	

	<div class="page-content">
		<div class="container-fluid">
        <header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Found</h2>
                            
							
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block" id="daftarkamar">
                    <button type="button" class="btn btn-inline btn-success" title="Tambah Data Kamar" id="btnTambah" onclick="showForm(true)">+</button>
					<table id="tbl_list" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>Types Found</th>
							<th>Description</th>
                            <th>Image</th>
							<th>Action</th></th>
						</tr>
						</thead>
						<tbody>		
						</tbody>
					</table>
				</div>
			</section>
            <section class="card">
            <form id="formTambah" method="POST">
                    <div class="box-typical box-typical-padding">

                    <input type="hidden" name="id_fakam" id="id_fakam">
		
                        <h5 class="m-t-lg with-border">Found</h5>

                        <div class="row">
                            <div class="col-lg-6">
                                <label class="form-label semibold">Add types found</label>
                                <select name="tipe_kamar" class="form-control" id="tipe_kamar">
                                    <?php
                                        while($row = $result->fetch_object()){
                                        
                                    ?>
                                    <option value="<?php echo $row->id_kamar ?>"><?php echo $row->tipe_kamar ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <fieldset class="form-group">
                                    <label class="form-label semibold" for="exampleInput">Description</label>
                                    <input type="text" class="form-control" id="fasilitas_kamar" name="fasilitas_kamar" placeholder="">
                                    
                                </fieldset>
                            </div>
                            <div class="col-lg-4">
                                <input type="file" name="gambar" id="gambar">
                            </div>
                            <div class="col-lg-6">
                                <button type="submit" class="btn btn-inline btn-success" id="btnSimpan">Simpan</button>
                                <button type="button" class="btn btn-inline btn-danger" onclick="closeForm()">Batal</button>
                            </div>
                            
                        </div><!--.row-->
			        </div><!--.box-typical-->
                </form>
            </section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php
    require_once("../layoutPartial/script_template.php");
?>

<script type="text/javascript" src="fasilitasKamar.js"></script>
	
</body>
</html>

<?php
    }else{
        header("Location:".BASE_URL);
    }
?>