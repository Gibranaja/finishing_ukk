<?php

	if($_SESSION["id_user"]==1){
	
?>

<nav class="side-menu">
	    <ul class="side-menu-list">
	        
	    
	        <li class="blue">
	            <a href="../kamar/index.php">
	                <i class="font-icon glyphicon glyphicon-paperclip"></i>
	                <span class="lbl">Data Kamar</span>
	            </a>
	        </li>
	        <li class="gold">
	            <a href="../fasilitasKamar/index.php">
	                <i class="font-icon font-icon-picture-2"></i>
	                <span class="lbl">Found</span>
	            </a>
	        </li>
	        <li class="red">
	            <a href="../fasilitasUmum/index.php">
	                <i class="font-icon font-icon-case-2"></i>
	                <span class="lbl">fasilitas Umum</span>
	            </a>
	        </li>
	        
	    </ul>
	
	    
	</nav><!--.side-menu-->
<?php
	}else{
?>

	<nav class="side-menu">
		<ul class="side-menu-list">
			
			<li class="grey with-sub">
				<a href="..\dataPemesan\index.php">
					<span class="font-icon font-icon-dashboard"></span>
					<span class="lbl">Data Pemesan</span>
				</a>
			</li>

		</ul>
	</nav><!--.side-menu-->
<?php
	}
?>
