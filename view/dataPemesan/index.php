<?php
    require_once '../../config/connection.php';
    session_start();
    if(isset($_SESSION["role"])){

    
?>
<?php
    require_once("../layoutPartial/head_template.php");
?>
<body class="with-side-menu">

<?php
    require_once("../layoutPartial/header.php");
?>
	

	<div class="mobile-menu-left-overlay"></div>
<?php
    require_once("../layoutPartial/nav.php");
?>
	

	<div class="page-content">
		<div class="container-fluid">
        <header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h2>Datakamar</h2>
                            
							
						</div>
					</div>
				</div>
			</header>
			<section class="card">
				<div class="card-block" id="daftarkamar">
					<table id="tbl_list" class="display table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>Nama Pemesan</th>
							<th>Email</th>
							<th>No Hp</th></th>
                            <th>Nama Tamu</th>
                            <th>Tipe Kamar</th>
                            <th>Check In</th>
                            <th>Check Out</th>
                            <th>Jumlah</th>
						</tr>
						</thead>
						<tbody>		
						</tbody>
					</table>
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->

<?php
    require_once("../layoutPartial/script_template.php");
?>

<script type="text/javascript" src="dataPemesan.js"></script>
	
</body>
</html>

<?php
    }else{
        header("Location:".BASE_URL);
    }
?>