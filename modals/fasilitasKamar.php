<?php

    require "../config/connection.php";

    Class FasilitasKamar
    {
        public function __construct()
        {}
        public function get_data()
        {
            $sql = "SELECT tb_fasilitas.id_fakam,
                    tb_fasilitas.tipe_kamar,
                    tb_fasilitas.fasilitas_kamar,
                    tb_kamar.tipe_kamar,
                    FROM tb_fasilitas INNER JOIN tb_kamar ON
                    tb_fasilitas.tipe_kamar = tb_kamar.id_kamar";
            return runQuery($sql);
        }

        public function insert($tipe_kamar, $fasilitas_kamar, $gambar)
        {
            $sql = "INSERT INTO tb_fasilitas (tipe_kamar, fasilitas_kamar, gambar) VALUES ('$tipe_kamar', '$fasilitas_kamar', '$gambar')";
            return runQuery($sql);
        }

        public function update($id_fakam, $tipe_kamar, $fasilitas_kamar, $gambar)
        {
            $sql = "UPDATE tb_fasilitas SET tipe_kamar='$tipe_kamar', fasilitas_kamar='$fasilitas_kamar', gambar='$gambar' WHERE id_fakam = '$id_fakam'";
            return runQuery($sql);
        }
        
        public function show($id_fakam)
        {
            $sql = "SELECT * FROM tb_fasilitas WHERE id_fakam='$id_fakam'";
            return runQueryRow($sql);
        }

        public function delete_data($id_fakam)
        {
            $sql = "DELETE FROM tb_fasilitas WHERE id_fakam='$id_fakam'";
            return runQuery($sql);
        }
    }