<?php

    require "../config/connection.php";

    Class FasilitasUmum
    {
        public function __construct()
        {}
        public function get_data()
        {
            $sql = "SELECT * FROM tb_fasilitas_umum";
            return runQuery($sql);
        }

        public function insert($nama_fasilitas, $keterangan)
        {
            $sql = "INSERT INTO tb_fasilitas_umum (nama_fasilitas, keterangan) VALUES ('$nama_fasilitas', '$keterangan')";
            return runQuery($sql);
        }

        public function update($id_fasilitas_umum, $nama_fasilitas, $keterangan)
        {
            $sql = "UPDATE tb_fasilitas_umum SET nama_fasilitas='$nama_fasilitas', keterangan='$keterangan' WHERE id_fasilitas_umum = '$id_fasilitas_umum'";
            return runQuery($sql);
        }
        
        public function show($id_fasilitas_umum)
        {
            $sql = "SELECT * FROM tb_fasilitas_umum WHERE id_fasilitas_umum='$id_fasilitas_umum'";
            return runQueryRow($sql);
        }

        public function delete_data($id_fasilitas_umum)
        {
            $sql = "DELETE FROM tb_fasilitas_umum WHERE id_fasilitas_umum='$id_fasilitas_umum'";
            return runQuery($sql);
        }
    }